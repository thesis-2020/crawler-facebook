# facebook-splash-scraper
Facebook Scraper Using Scrapy-Splash - Based on Huỳnh Ngọc Thiện Project, big respect


## Run

1. `pip install -r requirement.txt`

2. Run 2 lines in `./run_script.txt`

3. In `./facebook_splash_scrapper/setting.py` add groups link you want to crawl in list GROUPS like example below:

BUT, but ... active it 1 link at a time and run `scrapy crawl facebook_groups` on each link active

```
GROUPS = [
     "https://m.facebook.com/groups/example1" 
     #"https://m.facebook.com/groups/example2" 
     #"https://m.facebook.com/groups/example3"
]
```

Be careful with the link:

    1. It has to be `m.facebook`, not `www.facebook`
    2. It doesn't have character `/` at the end

4. Uncomment all group links you've just crawl from step 3 (example you run `scrapy crawl facebook_groups` 4 times for 4 links, uncomment exact those link)

5. Then simply run `sh exe.sh`

Your data will be stored in ./post/json/group/phrase2/ by each group

## Enhance

You can crawl the same group after a while to get new posts, it will remain the old data and update new posts of into group folder

## Important note:

If you didn't get `Crawled (200)` while running, check the cookie in `./cookies/cookie_test.json` and replace it by your own facebook cookie. This account facebook must join into private group before you add it into GROUPS setting.

How to get facebook cookie? You can get it by hand, they're in Cookie folder when you F12, or simply use J2Team tool. The format must follow the cookie json I've already created.

Any problem please fix it by yourself <3

## Thank you!