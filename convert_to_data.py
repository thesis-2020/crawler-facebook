import os
import json
from pre_process import *

class OneToOneConv:
    def __init__(self, id, post, link, input, output):
        self.id = id
        self.post = post
        self.link = link
        self.input = input
        self.output = output
        
    def get(self):
        return {'id': self.id, 'post': self.post, 'link': self.link, 'input': self.input, 'output': self.output}

acronym_list = []

def walk():
    one_to_one_conv = []
    start_id = 1
    count_conv_one_to_one = 0;
    count_conv_more_than_x = 0;

    # for root, dirs, files in os.walk('./posts/json/group/test_export_data'):
    # for root, dirs, files in os.walk('./posts/json/group/phrase2'):
    for root, dirs, files in os.walk('./posts/json/group/data_groups'):
        conv = []
        
        for file in files:
            with open(os.path.join(root, file), "r") as json_file:
                data = json.load(json_file)
                count_conv_more_than_x = count_conv_more_than_x + count_conv(data)
                # TODO:  handle long conversation
                [start_id, conv_list] = handle_one_to_one(start_id, data)
                one_to_one_conv = one_to_one_conv + conv_list     
    
    print(count_conv_more_than_x)
    export(one_to_one_conv)
    
def export(conv_list):
    with open('data.json', 'w+') as outfile:
        data = list(map(lambda x: x.get(), conv_list))
        json.dump(data, outfile, ensure_ascii=False, indent = 4)
        

def handle_one_to_one(start_id, item):
    conv_list = []
    post = item['post']
    link = item['link']
    
    for index, data in enumerate(item['post_comments']):
        parent_input = data['parent_comment']['parent_content']
        parent_output = data['replies'][0]['reply_content']
        if check_len(parent_input, parent_output):
            parent_input = replace_acronym(parent_input)
            parent_output = replace_acronym(parent_output)
            
            parent_conv = OneToOneConv(start_id, post, link, parent_input, parent_output)
            conv_list.append(parent_conv)
            start_id = start_id + 1
        
        if len(data['replies']) > 2:
            replies = iter(data['replies'][1:])
            for x, y in zip(replies, replies):
                input = x['reply_content']
                output = y['reply_content']
                
                if input and output and check_len(input, output):
                    input = replace_acronym(input)
                    output = replace_acronym(output) 
                    
                    conv = OneToOneConv(start_id, post, link, input, output)
                    conv_list.append(conv)
                    start_id = start_id + 1
                
    return [start_id, conv_list]
        

def check_len(input, output):
    if len(input) > 10 and len(output) > 25:
    #     input_word_list = list(filter(lambda x: len(x) < 3, input.split(' ')))[1:]
    #     output_word_list = list(filter(lambda x: len(x) < 3, output.split(' ')))[1:]
        
    #     for word in input_word_list:
    #         get_acronym(word) 
        
    #     for word in output_word_list:
    #         get_acronym(word) 
        
        return True
    return False

# def get_acronym(word):
#     if word not in acronym_list:
#         acronym_list.append(word) 

def count_conv(item):
    if len(item['post_comments']) > 3:
        return 1
    return 0

if __name__ == '__main__':
    walk()