# Scrapy settings for facebook_splash_scraper project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://docs.scrapy.org/en/latest/topics/settings.html
#     https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://docs.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'facebook_splash_scraper'

SPIDER_MODULES = ['facebook_splash_scraper.spiders']
NEWSPIDER_MODULE = 'facebook_splash_scraper.spiders'


# Crawl responsibly by identifying yourself (and your website) on the user-agent
# USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36'

# Obey robots.txt rules
ROBOTSTXT_OBEY = False

# Configure maximum concurrent requests performed by Scrapy (default: 16)
#CONCURRENT_REQUESTS = 32

# Configure a delay for requests for the same website (default: 0)
# See https://docs.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
#DOWNLOAD_DELAY = 3
# The download delay setting will honor only one of:
#CONCURRENT_REQUESTS_PER_DOMAIN = 16
#CONCURRENT_REQUESTS_PER_IP = 16

# Disable cookies (enabled by default)
#COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED = False

# Override the default request headers:
#DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
#}

# Enable or disable spider middlewares
# See https://docs.scrapy.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
#    'facebook_splash_scraper.middlewares.FacebookSplashScraperSpiderMiddleware': 543,
#}

# Enable or disable downloader middlewares
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
DOWNLOADER_MIDDLEWARES = {
    'scrapy_splash.SplashCookiesMiddleware': 723,
    'scrapy_splash.SplashMiddleware': 725,
    'scrapy.downloadermiddlewares.httpcompression.HttpCompressionMiddleware': 810,
    # 'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware': None,
    # 'scrapy.downloadermiddlewares.retry.RetryMiddleware': None,
    # 'scrapy_fake_useragent.middleware.RandomUserAgentMiddleware': 400,
    # 'scrapy_fake_useragent.middleware.RetryUserAgentMiddleware': 401,
    # 'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware': 500,
    # 'scrapy_useragents.downloadermiddlewares.useragents.UserAgentsMiddleware': 500,
}

# FAKEUSERAGENT_PROVIDERS = [
#     'scrapy_fake_useragent.providers.FakeUserAgentProvider',  # this is the first provider we'll try
#     'scrapy_fake_useragent.providers.FakerProvider',  # if FakeUserAgentProvider fails, we'll use faker to generate a user-agent string for us
#     'scrapy_fake_useragent.providers.FixedUserAgentProvider',  # fall back to USER_AGENT value
# ]

# FAKER_RANDOM_UA_TYPE = 'chrome'

# USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36'

# USER_AGENTS = [
#     ('Mozilla/5.0 (X11; Linux x86_64) '
#      'AppleWebKit/537.36 (KHTML, like Gecko) '
#      'Chrome/57.0.2987.110 '
#      'Safari/537.36'),  # chrome
#     ('Mozilla/5.0 (X11; Linux x86_64) '
#      'AppleWebKit/537.36 (KHTML, like Gecko) '
#      'Chrome/61.0.3163.79 '
#      'Safari/537.36'),  # chrome
#     ('Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) '
#      'Gecko/20100101 '
#      'Firefox/55.0')  # firefox
# ]

# Enable or disable extensions
# See https://docs.scrapy.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
#}

# Configure item pipelines
# See https://docs.scrapy.org/en/latest/topics/item-pipeline.html
#ITEM_PIPELINES = {
#    'facebook_splash_scraper.pipelines.FacebookSplashScraperPipeline': 300,
#}

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/autothrottle.html
#AUTOTHROTTLE_ENABLED = True
# The initial download delay
#AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED = True
#HTTPCACHE_EXPIRATION_SECS = 0
#HTTPCACHE_DIR = 'httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES = []
#HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'

HTTPCACHE_STORAGE = 'scrapy_splash.SplashAwareFSCacheStorage'

# DUPEFILTER_CLASS = 'scrapy.dupefilters.BaseDupeFilter'

DUPEFILTER_CLASS = 'scrapy_splash.SplashAwareDupeFilter'

# SPLASH
SPLASH_URL = 'http://0.0.0.0:8050'

COOKIES_ENABLED = True 
SPLASH_COOKIES_DEBUG = True
#
# FACEBOOK_ACCOUNT = [
#     {
#         "account": "",
#         "password": ""
#     }
# ]

SCROLLS = 500
SCROLLS_SEARCH = 100
GROUPS = [
    #  "https://m.facebook.com/groups/386973545401400" # nguoi viet lach                                #Done lan 1
    # "https://m.facebook.com/groups/371004360098732" # cau chuyen dien ro cua toi                      #Done lan 1
    # "https://m.facebook.com/groups/443921992911805" # tam su tu van tinh yeu                          # Done lan 1
    # "https://m.facebook.com/groups/307960726818551" #tam su tuoi pink                                 # Done lan 1
    # "https://m.facebook.com/groups/241769123373305" #tam su hoc duong                                 # Done lan 1   
    
    # "https://m.facebook.com/groups/weibovn" # weibo vn                                                #Done lan 1
    # "https://m.facebook.com/groups/2673981529591397" # sao nhieu nguoi co tinh yeu dep the #done lan 1 
    # 'https://m.facebook.com/groups/453758438752682' # bao lau chia tay
    # "https://m.facebook.com/groups/821166098451332" #cam xuc cua chung ta co the giong nhau            #Done lan 1
    # "https://m.facebook.com/groups/608021373466374" #chu tinh hay tru tinh                              #Done lan 1
    # "https://m.facebook.com/groups/190112834476976" #tu van tinh cam chia se cuoc song
    # "https://m.facebook.com/groups/nhungconnguoithattinh" #nhung con nguoi that tinh                  #Done lan 1
    # "https://m.facebook.com/groups/2030035913885813" #sot rac cam xuc                                 #Done lan 1
    # "https://m.facebook.com/groups/315154351880886" #chuyentrolinhtinh                                  #Done lan 1
    # "https://m.facebook.com/groups/330196354166717" #nhung con nguoi tot bung nhung hay buon             #Done lan 1
    # "https://m.facebook.com/groups/chuyencuato"  #chuyencuato                                         #Done lan 1
    # "https://m.facebook.com/groups/470834853742306" #hoinhungnguoirotnhieumonbachkhoa               # Done lan 1
    # "https://m.facebook.com/groups/2892075604199524" #tamsubuon
    # "https://m.facebook.com/groups/yeu1nguoioxa" #tamsuyeuxa
    # "https://m.facebook.com/groups/355450361193344" #fromf17withlove                                # Done lan 1
    # "https://m.facebook.com/groups/Emlanguoitotnhatthegian" #emlanguoitotnhatthegian  #????         # #Done lan 1
    # "https://m.facebook.com/groups/cauchuyencuatoi/" #cauchuyencuatoi
    # "https://m.facebook.com/groups/362531861014189/" "trochuyencungchuyengiatuvantamly"             # Done lan 1

    # "https://m.facebook.com/groups/492916974903571" #Hội những người bị rối loạn lưỡng cực/trầm cảm  #Done lan 1
    # "https://m.facebook.com/groups/mindsoul.vn"                                           #1
    # "https://m.facebook.com/groups/luanvetinhyeucungvivian"
    # "https://m.facebook.com/groups/304774881084110" #homnaycuabanthenao                   #1     ``
    # "https://m.facebook.com/groups/dongdangtramcam" #dongdangtramcam                      #1
    # "https://m.facebook.com/groups/Tuvan.edX" #tuvanEdx
    # "https://m.facebook.com/groups/tamlyhoccodienvahiendai"
    
    # "https://m.facebook.com/groups/821166098451332"
    # "https://m.facebook.com/groups/371004360098732"
    
    
    ##########
    # SEARCH #
    ##########

    # "https://m.facebook.com/groups/135559880239538"
    # "https://m.facebook.com/groups/461168094779608" #mindsoul.vn
    # "https://m.facebook.com/groups/304774881084110"
    # "https://m.facebook.com/groups/1535555256680542" #dongdangtramcam
    # "https://m.facebook.com/groups/304774881084110"
]

# GROUPS_SEARCH = [
#     "https://m.facebook.com/groups/135559880239538"
# ]

SEARCH_KEYWORDS = [
    "tâm sự",
    "thất tình",
    "đau khổ",
    "học hành",
    "người yêu",
    "gia đình",
    "chia sẻ",
    "chia tay",
    "lời khuyên",
    "crush"
]

PAGES = [
    "https://www.facebook.com/KTXDHQGConfessions",
    "https://www.facebook.com/HCMUTcfs",
    "https://www.facebook.com/neuconfessions",
    "https://www.facebook.com/Ftu2Confessions"
]
FACEBOOK_LINK = "https://m.facebook.com/"

NAME_GROUP = 'tamsuevagroup'
