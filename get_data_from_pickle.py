import pickle
import random

def get_pickle(filename):
	return pickle.load(open(filename, 'rb'))

vocab = get_pickle('vocabulary')

def translate_sent(sent):
	return [vocab[word] for word in sent]

# dev = get_pickle('dev')
outputs = get_pickle('outputs')
train = get_pickle('train')

def get_output(output_id):
	return translate_sent(outputs[output_id])

# for data_item in outputs:
#     print(data_item)
# print(random.sample(outputs.values(), 5))
good_answers = []
questions = []
for i, q in enumerate(train):
    questions += [q['input']] * len(q['output'])
    good_answers += [outputs[j] for j in q['output']]

print(len(good_answers))

# pickle.dump(train, open("train_py2","wb"), protocol=2)
# pickle.dump(outputs, open("outputs_py2","wb"), protocol=2)
# pickle.dump(vocab, open("vocabulary_py2","wb"), protocol=2)


# for data_item in train:
#     print('Input:', translate_sent(data_item['input']))
#     print('Good Output:', get_output(data_item['output'][0]))

# for data_item in dev:
# 	for bad_answer in data_item['bad']:
# 		print('Question:', translate_sent(data_item['question']))
# 		print('Good Answer:', get_answer(data_item['good'][0]))
# 		print('Bad Answer: ', get_answer(bad_answer), '\n============')
