import json
from functools import reduce
import pickle

vocabulary = {}

def walk():
    input_list = []
    output_list = []
    all_data = list()
    output_data_list = list()
    with open('./data.json', 'r') as jsonfile:
    # with open('./data_test.json', 'r') as jsonfile:
        data = json.load(jsonfile)
        
        # get_vocab(data)
        
        # ========================
        
        input_list = reduce(lambda x, y: x + [y['input']], data, [])
        output_list =  reduce(lambda x, y: x + [y['output']], data, [])
        
        label_output_list = [output_list.index(i) for i in output_list]
        
        input_list = reduce(lambda a, x: a + [reduce(lambda y, z: y + [list(vocabulary.keys())[list(vocabulary.values()).index(z)]], list(filter(lambda m: m != '', x.split(' '))), [])], input_list, [])
        output_list = reduce(lambda a, x: a + [reduce(lambda y, z: y + [list(vocabulary.keys())[list(vocabulary.values()).index(z)]], list(filter(lambda m: m != '', x.split(' '))), [])], output_list, [])
        output_list_copy = output_list.copy()
        
        for input in input_list:
            if (len(input) == 0):
                continue
            same_input = [i for i, x in enumerate(input_list) if x == input]
            same_output = list()
            for i in same_input:
                same_output.append(label_output_list[i])
            for i in same_input[1:]:
                input_list[i] = []
                output_list[i] = []
            all_data.append({'input': input_list[same_input[0]], 'output': same_output})
            
            
        for output in label_output_list:
            if (output == -1):
                continue
            same_output = [i for i, x in enumerate(label_output_list) if x == output]
            for i in same_output[1:]:
                label_output_list[i] = -1 
            output_data_list.append(output_list_copy[same_output[0]])
    
    label_output = list(dict.fromkeys(label_output_list))
    label_output = [x for x in label_output if x != -1]
    
    output_data = { label_output[i] : output_data_list[i] for i in range(0, len(label_output) ) } 
    
    with open('train', 'wb') as handle:
        pickle.dump(all_data, handle, protocol=pickle.HIGHEST_PROTOCOL)
                       
    with open('outputs', 'wb') as handle:
        pickle.dump(output_data, handle, protocol=pickle.HIGHEST_PROTOCOL)
        
def get_vocab(data):
    vocab_list = []
    for conv in data:
        for text in (conv['input'] + ' ' + conv['output']).split(' '):
            if text not in vocab_list:
                vocab_list.append(text)
     
    vocab = { i : vocab_list[i] for i in range(0, len(vocab_list) ) } 
    
    with open('vocabulary', 'wb') as handle:
        pickle.dump(vocab, handle, protocol=pickle.HIGHEST_PROTOCOL)
    

def get_pickle(filename):
	return pickle.load(open(filename, 'rb'))

def translate_sent(sent):
    return [vocabulary[word] for word in sent]


if __name__ == '__main__':
    vocabulary = get_pickle('vocabulary')
    walk()