# =========================================================================
#  Title:  Extract neede features through xpath from posts html files that
#  Author: Huỳnh Ngọc Thiện
#  Date:   Jan 9 2021
# =========================================================================

import lxml.html
from scrapy.utils.project import get_project_settings
import json
import os
import time
from datetime import datetime, timedelta
from functools import reduce

# This will setup settings variable to get constant from settings.py such as SCROLLS (scrolling number)

settings = get_project_settings()

# Get groups list

groups = settings.get("GROUPS")
NAME_GROUP = settings.get("NAME_GROUP")

for url in groups:

    group = str(url.split("/")[-1])

    with open("./groups/json/temp/group_posts_" + group + '.json', 'r') as jsonfile:
        posts = json.load(jsonfile)
        
    for post in posts:
        # Check if post was crawled and stored into html file or not

        if os.path.isfile("./posts/html/post_html_" + post["post"] + '.html'):
            htmls = ""
            with open("./posts/html/post_html_" + post["post"] + '.html', 'r') as f:
                htmls = f.read()

            # Parse string html in file into xpath objects

            if htmls == "":
                os.remove("./posts/html/post_html_" + post["post"] + '.html')
                continue
            
            path = "./posts/json/group/" + "data_groups/" +  group
            if not os.path.isdir(path):
                os.makedirs(path)

            path_backup = "./posts/backup/html/" +  group
            if not os.path.isdir(path_backup):
                os.makedirs(path_backup)

            htmls = lxml.html.fromstring(str(htmls))
            
            post_message = htmls.xpath("//div[@class='kr9hpln1']")
                        
            content = []
            
            # print(htmls.xpath("//div[@class='o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q']//text()"))
            
            if post_message == []:
                content = content + htmls.xpath("//div[@class='kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q']//text()")
                content = content + htmls.xpath("//div[@class='o9v6fnle cxmmr5t8 oygrvhab hcukyx3x c1et5uql ii04i59q']//text()")
            else:
                content = post_message[0].xpath(".//text()")

            content_list = []
            for i in content:
                content_list = content_list + list(filter(lambda x: len(x) > 100, i.split('.')))
                
            # print(content_list)
            comments = htmls.xpath("//div[@class='cwj9ozl2 tvmbv18p']/ul/li")

            post["post_comments"] = []        

            for comment in comments:            
                parent_content = comment.xpath(".//div[@class='l9j0dhe7 ecm0bbzt rz4wbd8a qt6c0cv9 dati1w0a j83agx80 btwxx1t3 lzcic4wl']//div[@class='ecm0bbzt e5nlhep0 a8c37x1j']//text()")
                parrent_user = comment.xpath(".//div[@class='l9j0dhe7 ecm0bbzt rz4wbd8a qt6c0cv9 dati1w0a j83agx80 btwxx1t3 lzcic4wl']//span[@class='d2edcug0 hpfvmrgz qv66sw1b c1et5uql b0tq1wua a8c37x1j keod5gw0 nxhoafnm aigsh9s9 tia6h79c fe6kdd0r mau55g9w c8b282yb iv3no6db e9vueds3 j5wam9gi lrazzd5p oo9gr5id']//text()")[0]
                parent_content = reduce(lambda x, y: x + y, parent_content, '')
                

                parent_comment = {'parent_user': parrent_user, 'parent_content': parent_content}
                
                replies = comment.xpath(".//div[@class='kvgmc6g5 jb3vyjys rz4wbd8a qt6c0cv9 d0szoon8']")
                replies_list = []
                
                reply_first_user = ''
                reply_previous_user = ''
                
                for index in range(0,len(replies)):
                    reps = replies[index].xpath("./ul/li")
                    if len(reps) > 0:
                        for index, rep in enumerate(reps):
                            
                            reply_content = rep.xpath(".//div[@class='ecm0bbzt e5nlhep0 a8c37x1j']//text()")
                            reply_user = rep.xpath(".//span[@class='d2edcug0 hpfvmrgz qv66sw1b c1et5uql b0tq1wua a8c37x1j keod5gw0 nxhoafnm aigsh9s9 tia6h79c fe6kdd0r mau55g9w c8b282yb iv3no6db e9vueds3 j5wam9gi lrazzd5p oo9gr5id']//text()")[0]
                                       
                            if (index == 0):
                                reply_first_user = reply_user
                            else:
                                if (reply_user not in [parrent_user, reply_first_user] or reply_user == reply_previous_user):
                                    continue
                                
                                
                            reply_previous_user = reply_user
                                    
                            if reply_user == reply_first_user:                                         
                                reply_content = reduce(lambda x, y: x + y.replace(parrent_user, ''), reply_content, '')
                            elif reply_user == parrent_user:
                                reply_content = reduce(lambda x, y: x + y.replace(reply_first_user, ''), reply_content, '')
                            
                        
                            replies_list.append({'reply_user': reply_user, 'reply_content': reply_content})
                
                if len(replies_list) > 0:
                    post["post_comments"].append({'parent_comment': parent_comment, 'replies': replies_list})
            
                comment_reaction = comment.xpath(".//div[@class='_680y']//span[@class='m9osqain e9vueds3 knj5qynh j5wam9gi jb3vyjys n8tt0mok qt6c0cv9 hyh9befq g0qnabr5']/text()")         
            
                if len(comment_reaction) > 0:
                    if (int(comment_reaction[0]) < 1):
                        continue
                else:
                    continue   
                
                for c in content_list:
                    if len(parent_comment['parent_content']) > 3:
                        post["post_comments"].append({'parent_comment': {'parent_content': c}, 'replies': [{'reply_content': parent_comment['parent_content']}]})

                        
            if not len(post["post_comments"]):
                os.remove("./posts/html/post_html_" + post["post"] + '.html')
                continue
            
            path_new_json = path + "/post_" + post["post"] + '.json'
            
            if not os.path.exists(path_new_json):
                with open(path_new_json, 'w+') as jsonfile:
                    json.dump(post, jsonfile, ensure_ascii=False, indent = 4)
        
            os.rename("./posts/html/post_html_" + post["post"] + '.html', path_backup + '/post_html_' + post["post"] + '.html')
            