from acronym import ACRONYM_LIST
import re

def correct_spelling(text):
    corrected = ''
    # TODO: call Viettel API
    return corrected

def replace_acronym(text):
    for acronym in ACRONYM_LIST:
        text = text.lower()
        text = re.sub(acronym[0], acronym[1], text, flags=re.IGNORECASE)
        text = text.strip()
        # print(text)
        
    return text