import json
import os
def data_correction():
    with open(os.path.join(os.getcwd(),'data.json')) as json_file:
        data = json.load(json_file)
        len(data)
        total = len(data)//10
        for i in range(11):
            with open(os.path.join(os.getcwd(),'data_' + str(i) + '.json'),'w+', encoding='utf-8') as corrected:
                json.dump(data[i*total:(i+1)*total],corrected, ensure_ascii=False, indent=4)


if __name__ == "__main__":
    data_correction()