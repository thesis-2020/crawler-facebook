import requests
import json,os
import _thread

def correction(text):
    r = requests.post("https://viettelgroup.ai/nlp/api/v1/spell-checking", headers={'content-type': 'application/json','token': 'BBd-4VC-vK1Q3QGErwehUhLxRIELTMVeE3SRkvrjf8IgK-E0tCjw3BtwvwR1iTid'}, data = json.dumps({ 'sentence':text}))
    data = r.json()
    for suggestion in data['result']['suggestions']:
        text = text[:suggestion['startIndex']]+suggestion['suggestion']+text[suggestion['endIndex']:]
    return text

def data_correction(fileName):
    print("ok")
    with open(os.path.join(os.getcwd(),fileName)) as json_file:
        data = json.load(json_file)
        for io_text in data:
            io_text['input']=correction(io_text['input'])
            io_text['output']=correction(io_text['output'])
            print("corrected: ", io_text['id'])
        with open(os.path.join(os.getcwd(),'corrected'+fileName),'w+', encoding='utf-8') as corrected:
            json.dump(data,corrected, ensure_ascii=False, indent=4)

try:
    for i in range(11):
        print('data_'+str(i) +'.json')
        _thread.start_new_thread( data_correction, ('data_'+str(i) +'.json',) )
except:
    print("Error: unable to start thread")

while 1:
   pass